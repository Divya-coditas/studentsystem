const popup = document.querySelector(".popupwindow")
const yesDeletebutton = document.querySelector(".yesbutton")
const noDeletebutton = document.querySelector(".nobutton")
const container1 = document.querySelector(".container")
async function Deleteusers() {
    try {
        let response = await fetch(`https://5202-1-22-124-182.in.ngrok.io/showStudents`, {
            headers: new Headers({
                "ngrok-skip-browser-warning": "1234",
            })
        });
        let userDetails = await response.json();
        userDetails.forEach(e => {
            const deletebtn = document.getElementById(`${e.studentEnrollmentId}`)
            deletebtn.addEventListener("click", () => {
                container1.style.display = "none"
                popup.classList.remove("active");
                yesDeletebutton.addEventListener("click", () => {
                    fetch(`http://d4ac-103-51-153-190.ngrok.io/deleteStudent/${e.studentEnrollmentId}`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(e.studentEnrollmentId)
                    })
                    popup.classList.remove("active");
                    window.location.reload()
                });
            }
            )
            noDeletebutton.addEventListener("click", () => {
                popup.classList.add("active");
                container1.style.display = ""
            });
        })
    }
    catch {
    }
}
export default {
    Deleteusers
}




















